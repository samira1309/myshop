@extends('layouts.site.master')
@section('content')
   
<main>
<!-- =======================
Main Banner START -->
<section class="position-relative overflow-hidden pb-0 pb-sm-5">

	<!-- SVG decoration -->
	<figure class="ms-5 position-absolute top-0 start-0">
		<svg width="29px" height="29px">
			<path class="fill-orange opacity-4" d="M29.004,14.502 C29.004,22.512 22.511,29.004 14.502,29.004 C6.492,29.004 -0.001,22.512 -0.001,14.502 C-0.001,6.492 6.492,-0.001 14.502,-0.001 C22.511,-0.001 29.004,6.492 29.004,14.502 Z"></path>
		</svg>
	</figure>

	<!-- Content START -->
	<div class="container">
		<div class="row align-items-center justify-content-xl-between g-4 g-md-5">
			<!-- Left content START -->
			<div class="col-lg-7 col-xl-5 position-relative z-index-1 text-center text-lg-start mb-2 mb-md-9 mb-xl-0">
				<!-- Badge -->
				<h6 class="mb-3 font-base bg-primary bg-opacity-10 text-primary py-2 px-4 rounded-2 d-inline-block">با ما شروع کنید</h6>
				<!-- Title -->
				<h1 class="mb-4 display-6">به دانشگاه آنلاین ما
					<span class="position-relative d-inline-block">خوش آمدید
						<!-- SVG decoration -->
						<span class="position-absolute top-50 start-50 translate-middle z-index-n1 d-none d-sm-block">
							<svg width="387.7px" height="119.7px">
								<path class="fill-warning" d="M382.7,51.4c-0.2-1-0.4-2-0.7-3c-0.2-0.6-0.5-1.2-0.9-1.7c-0.6-0.9-1.5-1.7-2.9-2.2l0.1-0.1l-0.1,0.1 c0.2-0.9-0.4-1.2-1.2-1.3c-0.1,0-0.2,0-0.4-0.1c-0.2-0.2-0.5-0.5-0.7-0.7c0-0.7-0.1-1.3-0.6-1.7c-0.3-0.2-0.7-0.4-1.3-0.5 c0-0.7-0.2-1.1-0.6-1.4c-0.3-0.2-0.7-0.4-1.2-0.5c-0.2,0-0.3-0.1-0.5-0.1c-1.1-0.9-2.2-1.8-3.4-2.7c0-0.1,0-0.2-0.1-0.3 c-0.1-0.2-0.3-0.4-0.7-0.4c-2.1-1.2-4.2-2.3-6.2-3.5c-14.1-8.5-31.1-10.2-46.8-14.7c-9.6-2.7-19.8-3.4-29.8-4.7 c-13.3-1.8-26.7-1.5-40-2.5c-5.4-0.4-10.8-0.7-16.1-0.7c-2.8,0-5.7-0.6-8.3-0.2c-5.8,0.9-11.6,1.5-17.4,1.8c-2,0.1-3.9,0.2-5.9,0.2 c-0.2,0-0.3,0-0.5,0.1c-0.2,0-0.3,0-0.5,0.1c-2.1,0-4.3,0.1-6.4,0.2c-2.1,0.1-4.3,0.1-6.4-0.1c-13-0.8-25.3,1.7-37.8,3.5 c-6,0.9-11.9,2.2-17.9,3.5c-6.5,1.4-13.3,1.7-19.8,3.3c-9.6,2.3-19.3,4.4-29.1,6c-9.5,1.6-18.9,3.9-28.2,6.4 c-8.5,2.3-16.2,5.9-23.8,9.7c-4.4,2.2-9,4.1-12.4,7.6c-4.1,4.3-6.6,9.4-10,14.1C1.9,68,2.5,70.8,4.6,74c4.7,7.3,12.9,10.3,21.3,13.4 c4.1,1.5,8.6,2.4,12.5,4.3c5.5,2.6,10.9,5.4,16.7,7.6c12.3,4.6,25.1,8,38.1,10.5c7.1,1.4,14.5,2.1,21.8,2.6 c11.2,0.8,22.5,2.5,33.8,1.9c0.8,0.7,1.5,0.7,2.1-0.1c1.6-0.7,3.4,0.2,5.1-0.1c8.8-1.5,17.8-0.8,26.8-0.6c5,0.1,10.1,0.8,15.1,0.6 c9.4-0.4,18.8-1,28.2-1.9c12.9-1.2,25.7-2.4,38.2-5.3c0.3,0.4,0.5,0.3,0.6-0.1c1.1-0.2,2.3-0.4,3.4-0.6c0.3,0.3,0.5,0.2,0.7-0.1 c1.2-0.3,2.4-0.6,3.7-0.8c7.9-0.8,15.8-1.4,23.6-2.4c4.9-0.6,9.7-1.8,14.5-2.8c0.4,0.2,0.8,0.3,1.1,0.2c0.2,0,0.3-0.1,0.4-0.2 c0.1-0.1,0.1-0.1,0.2-0.2s0.1-0.2,0.2-0.2c0.5-0.1,1-0.3,1.5-0.4c0.1,0,0.2,0.1,0.3,0.1c0.3,0,0.5-0.1,0.6-0.4c0,0,0,0,0,0l0,0 c0,0,0,0,0,0c0.4-0.1,0.8-0.2,1.3-0.3c0,0,0,0,0.1,0c0.2,0,0.3,0,0.5,0c0.4,0,0.7-0.2,0.8-0.7c1.1-0.4,2.2-0.8,3.3-1.3 c0.2,0.1,0.4,0.1,0.6,0c0.1,0,0.2-0.1,0.2-0.1c0.1-0.1,0.1-0.1,0.2-0.2c0-0.1,0.1-0.1,0.2-0.1c0.1,0,0.1,0,0.2,0 c0.6,0.2,1,0.2,1.4,0c0.2-0.1,0.3-0.2,0.5-0.4c0.1-0.2,0.2-0.4,0.3-0.6c1.2-0.5,2.4-1,3.7-1.6c3.7-1.6,7.3-3.3,11.1-4.4 c11.2-3.4,21.5-7.9,30.2-14.9c1.8-0.4,2.9-1.2,3.7-2.4c0.5-0.7,0.8-1.4,1.1-2.2c1.1-0.1,1.7-0.6,2.1-1.1c0.4-0.6,0.6-1.3,0.7-2 c0-0.1,0.1-0.2,0.2-0.3c1.1,0.1,1.4-0.7,1.8-1.3C382.2,61.1,383.8,56.5,382.7,51.4z M9.5,72.3c-0.4-0.9-0.8-2-0.2-2.9 c4.3-6.9,8-14.3,15.9-19c6.6-3.9,13.9-6.9,21.1-10c10.1-4.3,21.1-6,32-8.1c0,0.2,0,0.4,0.1,0.6l0,0c-2.5,0.9-5.1,1.7-7.7,2.6 c-7.7,2.5-15.4,5-22.9,7.9c-10,3.9-18.1,9.9-23.8,17.8c-1.2,1.6-2.5,3.1-3.7,4.6c-5.1,6.3-2.3,11,2.9,16.4c0.3,0.3,0.7,0.7,0.9,1.1 C17.6,81.2,12,78.2,9.5,72.3z M372.5,60.6c-4,6.6-9.6,11.9-16.6,16.1c-4.8,2.9-10.5,5-16.2,6.8c-7.8,2.5-15.1,5.6-22.5,8.6 c-9.3,3.8-19,5.9-29.3,6.8c-14.1,1.2-27.8,3.6-41.6,5.9c-11.4,2-23.2,2.4-34.8,3.6c-13.2,1.4-26.4,0.4-39.6,0.2 c-7.4-0.1-14.8,0.8-22.1,1.2c-6.1,0.4-12.2,0.3-18.3-0.2c-9.2-0.7-18.5-1.3-27.7-2.2c-6.5-0.6-13.1-1.7-19.4-3.4 c-7.5-2-14.9-4-22.4-6c-1.2-0.3-2.3-0.6-3.2-1.3c-0.5-0.2-0.9-0.4-1.5-0.6c0.1,0,0.2-0.1,0.3-0.1c0.7-0.2,1.2,0,1.8,0.2c0,0,0,0,0,0 c8.1,1.1,16.2,2.8,24.4,3.2c1.2,0.1,2.4,0.1,3.5,0.1c1.1,0,3,0.5,3.1-0.6c0.1-1.4-1.8-2-3.3-2c-5,0-9.9-0.5-14.8-1.2 c-10.8-1.6-21.5-3.4-31.6-7.2c-6.9-2.5-12.7-6.4-16.2-12.3c-1.1-1.9-1.2-3.7-0.2-5.7c7.6-14.6,21.3-23.3,38.6-28.9 c15.7-5.1,31.3-10.6,47.6-14.2c11.7-2.6,23.7-4.3,35.3-6.9c20-4.5,40.6-5.7,61.3-6.4c8.5-0.3,16.8-2,25.4-1.3 c19.7,1.6,39.4,2.8,59.1,5.5c10.6,1.5,21.4,2.9,32.1,4c8.4,0.8,16.8,3.3,24.8,6.5c7.4,3,14.1,6.8,20,11.7 C374.7,46.2,376.4,54.2,372.5,60.6z"/>
							</svg>
						</span>
					</span>
				</h1>
				<!-- Content -->
				<p class="mb-3">معتبرترین دوره های آنلاین و گواهینامه های بازاریابی، فناوری اطلاعات، برنامه نویسی و بانک های اطلاعاتی.
				</p>
				<!-- Search bar -->
				<form class="border rounded p-2 mb-4">
					<div class="input-group">
						<input class="form-control border-0 me-1" type="search" placeholder="جستجو...">
						<button type="button" class="btn btn-primary mb-0 rounded"><i class="fas fa-search"></i></button>
					</div>
				</form>
				
				<!-- Counter START -->
				<div class="row g-3 mb-3 mb-lg-0">
					<!-- Item -->
					<div class="col-sm-6">
						<div class="d-flex align-items-center">
							<!-- Icon -->
							<div class="icon-lg fs-4 text-orange bg-orange bg-opacity-10 rounded"> <i class="bi bi-book-half"></i> </div>
							<!-- Info -->
							<div class="ms-3">
								<div class="d-flex">
									<h4 class="purecounter fw-bold mb-0" data-purecounter-start="0" data-purecounter-end="600" data-purecounter-delay="100">0</h4>
									<span class="h4 mb-0">+</span>
								</div>
								<div>دوره آنلاین</div>
							</div>
						</div>
					</div>
					<!-- Item -->
					<div class="col-sm-6">
						<div class="d-flex align-items-center">
							<!-- Icon -->
							<div class="icon-lg fs-4 text-info bg-info bg-opacity-10 rounded"> <i class="fas fa-university"></i> </div>
							<!-- Info -->
							<div class="ms-3">
								<div class="d-flex">
									<h4 class="purecounter fw-bold mb-0" data-purecounter-start="0" data-purecounter-end="400" data-purecounter-delay="100">0</h4>
									<span class="h4 mb-0">+</span>
								</div>
								<div>حوزه تخصصی</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Counter END -->
			</div>
			<!-- Left content END -->

			<!-- Right content START -->
			<div class="col-lg-5 col-xl-6 text-center position-relative">
				<!-- SVG decoration -->
				<figure class="position-absolute top-100 start-0 translate-middle mt-n6 ms-5 ps-5 d-none d-md-block">
					<svg width="297.5px" height="295.9px">
					<path stroke="#F99D2B" fill="none" stroke-width="13" d="M286.2,165.5c-9.8,74.9-78.8,128.9-153.9,120.4c-76-8.6-131.4-78.2-122.8-154.2C18.2,55.8,87.8,0.3,163.7,9"></path>
					</svg>
				</figure>

				<!-- Bell icon -->
				<div class="icon-lg bg-primary text-white rounded-4 shadow position-absolute top-0 start-100 translate-middle z-index-9 ms-n4 d-none d-md-block">
					<i class="fas fa-bell"></i>
				</div>

				<!-- Live class -->
				<div class="p-3 card card-body shadow rounded-4 position-absolute top-0 start-0 translate-middle ms-5 z-index-1 d-none d-xl-block">
					<div class="d-flex justify-content-between">
						<!-- Avatar -->
						<div class="avatar">
							<img class="avatar-img rounded-circle shadow" src="assets/images/avatar/04.jpg" alt="avatar">
						</div>
						<!-- Info -->
						<div class="text-start ms-3">
							<h6 class="mb-0">آموزش پیشرفته جاوا Java</h6>
							<p class="mb-0 small text-body">امروز دقیقه 13:00</p>
							<!-- Button -->
							<a href="#" class="btn btn-sm btn-danger-soft mt-2 mb-0">ثبت نام</a>
						</div>
					</div>
				</div>

				<!-- Student review -->
				<div class="position-absolute top-100 start-100 translate-middle z-index-1 ms-n6 mt-n5 d-none d-xxl-block">
					<div class="card card-body shadow p-4 rounded-3 d-inline-block position-relative z-index-1">
						<!-- Icon -->
						<div class="icon-lg bg-success rounded-circle position-absolute top-100 start-100 translate-middle">
							<i class="bi bi-chat-left-text text-white"></i>
						</div>
						<!-- Avatar list -->
						<ul class="avatar-group mb-2">
							<li class="avatar avatar-sm">
								<img class="avatar-img rounded-circle" src="assets/images/avatar/01.jpg" alt="avatar">
							</li>
							<li class="avatar avatar-sm">
								<img class="avatar-img rounded-circle" src="assets/images/avatar/02.jpg" alt="avatar">
							</li>
							<li class="avatar avatar-sm">
								<img class="avatar-img rounded-circle" src="assets/images/avatar/03.jpg" alt="avatar">
							</li>
							<li class="avatar avatar-sm">
								<div class="avatar-img rounded-circle bg-primary">
									<span class="text-white position-absolute top-50 start-50 translate-middle">+</span>
								</div>
							</li>
						</ul>
						<!-- Title -->
						<h6 class="mb-1 text-start font-base">15 شرکت کننده</h6>
						<!-- Review -->
						<ul class="list-inline mb-0">
							<li class="list-inline-item me-1 h6 mb-0">4.5</li>
							<li class="list-inline-item me-0 small"><i class="fas fa-star text-warning"></i></li>
							<li class="list-inline-item me-0 small"><i class="fas fa-star text-warning"></i></li>
							<li class="list-inline-item me-0 small"><i class="fas fa-star text-warning"></i></li>
							<li class="list-inline-item me-0 small"><i class="fas fa-star text-warning"></i></li>
							<li class="list-inline-item me-0 small"><i class="fas fa-star-half-alt text-warning"></i></li>
						</ul>
					</div>
				</div>

				<div class=" position-relative">
					<!-- Yellow background -->
					<div class="bg-warning rounded-4 border border-white border-5 h-200px h-sm-300px shadow"></div>
					<!-- Image -->
					<img src="assets/images/element/06.png" class="position-absolute bottom-0 start-50 translate-middle-x"  alt="">
				</div>

			</div>
			<!-- Right content END -->
		</div>
	</div>
	<!-- Content END -->
</section>
<!-- =======================
Main Banner END -->

<!--Category START -->
<section>
	<div class="container">
		<div class="row g-4">

			<!-- Category item -->
			<div class="col-sm-6 col-lg-4 col-xl-3">
				<div class="card card-body shadow rounded-3">
					<div class="d-flex align-items-center">
						<!-- Icon -->
						<div class="icon-lg bg-purple bg-opacity-10 rounded-circle text-purple"><i class="fas fa-tools"></i></div>
						<div class="ms-3">
							<h5 class="mb-0 fw-normal"><a href="#" class="stretched-link">مهندسی عمران</a></h5>
							<span>89 دوره</span>
						</div>
					</div>
				</div>
			</div>

			<!-- Category item -->
			<div class="col-sm-6 col-lg-4 col-xl-3">
				<div class="card card-body shadow rounded-3">
					<div class="d-flex align-items-center">
						<!-- Icon -->
						<div class="icon-lg bg-danger bg-opacity-10 rounded-circle text-danger"><i class="fas fa-heartbeat"></i></div>
						<div class="ms-3">
							<h5 class="mb-0 fw-normal"><a href="#" class="stretched-link">پزشکی و سلامت</a></h5>
							<span>95 دوره</span>
						</div>
					</div>
				</div>
			</div>

			<!-- Category item -->
			<div class="col-sm-6 col-lg-4 col-xl-3">
				<div class="card card-body shadow rounded-3">
					<div class="d-flex align-items-center">
						<!-- Icon -->
						<div class="icon-lg bg-blue bg-opacity-10 rounded-circle text-blue"><i class="fas fa-photo-video"></i></div>
						<div class="ms-3">
							<h5 class="mb-0 fw-normal"><a href="#" class="stretched-link">طراحی گرافیک</a></h5>
							<span>38 دوره</span>
						</div>
					</div>
				</div>
			</div>

			<!-- Category item -->
			<div class="col-sm-6 col-lg-4 col-xl-3">
				<div class="card card-body shadow rounded-3">
					<div class="d-flex align-items-center">
						<!-- Icon -->
						<div class="icon-lg bg-success bg-opacity-10 rounded-circle text-success"><i class="fas fa-laptop-code"></i></div>
						<div class="ms-3">
							<h5 class="mb-0 fw-normal"><a href="#" class="stretched-link">برنامه نویسی</a></h5>
							<span>105 دوره</span>
						</div>
					</div>
				</div>
			</div>

			<!-- Category item -->
			<div class="col-sm-6 col-lg-4 col-xl-3">
				<div class="card card-body shadow rounded-3">
					<div class="d-flex align-items-center">
						<!-- Icon -->
						<div class="icon-lg bg-orange bg-opacity-10 rounded-circle text-orange"><i class="fas fa-crop-alt"></i></div>
						<div class="ms-3">
							<h5 class="mb-0 fw-normal"><a href="#" class="stretched-link">طراحی سایت</a></h5>
							<span>72 دوره</span>
						</div>
					</div>
				</div>
			</div>

			<!-- Category item -->
			<div class="col-sm-6 col-lg-4 col-xl-3">
				<div class="card card-body shadow rounded-3">
					<div class="d-flex align-items-center">
						<!-- Icon -->
						<div class="icon-lg bg-primary bg-opacity-10 rounded-circle text-primary"><i class="fas fa-business-time"></i></div>
						<div class="ms-3">
							<h5 class="mb-0 fw-normal"><a href="#" class="stretched-link">استارت آپ</a></h5>
							<span>68 دوره</span>
						</div>
					</div>
				</div>
			</div>

			<!-- Category item -->
			<div class="col-sm-6 col-lg-4 col-xl-3">
				<div class="card card-body shadow rounded-3">
					<div class="d-flex align-items-center">
						<!-- Icon -->
						<div class="icon-lg bg-info bg-opacity-10 rounded-circle text-info"><i class="fas fa-music"></i></div>
						<div class="ms-3">
							<h5 class="mb-0 fw-normal"><a href="#" class="stretched-link">موزیک</a></h5>
							<span>51 دوره</span>
						</div>
					</div>
				</div>
			</div>

			<!-- Category item -->
			<div class="col-sm-6 col-lg-4 col-xl-3">
				<div class="card card-body shadow rounded-3">
					<div class="d-flex align-items-center">
						<!-- Icon -->
						<div class="icon-lg bg-warning bg-opacity-15 rounded-circle text-warning"><i class="fas fa-palette"></i></div>
						<div class="ms-3">
							<h5 class="mb-0 fw-normal"><a href="#" class="stretched-link">هنر</a></h5>
							<span>69 دوره</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- =======================
Category END -->

<!-- =======================
Featured course START -->
<section class="pt-0 pt-md-5">
	<div class="container">
		<!-- Title -->
		<div class="row mb-4">
			<div class="col-lg-8 text-center mx-auto">
				<h2 class="fs-3 mb-0">دوره های ویژه جشنواره</h2>
				<p class="mb-0">برترین‌های هفته را کاوش کنید</p>
			</div>
		</div>

		<div class="row g-4">
			<!-- Card Item START -->
			<div class="col-md-6 col-lg-4 col-xxl-3">
				<div class="card p-2 shadow h-100">
					<div class="rounded-top overflow-hidden">
						<div class="card-overlay-hover">
							<!-- Image -->
							<img src="assets/images/courses/4by3/17.jpg" class="card-img-top" alt="course image">
						</div>
						<!-- Hover element -->
						<div class="card-img-overlay">
							<div class="card-element-hover d-flex justify-content-end">
								<a href="#" class="icon-md bg-white rounded-circle">
									<i class="fas fa-shopping-cart text-danger"></i>
								</a>
							</div>
						</div>
					</div>
					<!-- Card body -->
					<div class="card-body px-2">
						<!-- Badge and icon -->
						<div class="d-flex justify-content-between">
							<!-- Rating and info -->
							<ul class="list-inline hstack gap-2 mb-0">
								<!-- Info -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-orange bg-opacity-10 text-orange rounded-circle"><i class="fas fa-user-graduate"></i></div>
									<span class="h6 fw-light mb-0 ms-2">9.1k</span>
								</li>
								<!-- Rating -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-warning bg-opacity-15 text-warning rounded-circle"><i class="fas fa-star"></i></div>
									<span class="h6 fw-light mb-0 ms-2">4.5</span>
								</li>
							</ul>
							<!-- Avatar -->
							<div class="avatar avatar-sm">
								<img class="avatar-img rounded-circle" src="assets/images/avatar/09.jpg" alt="avatar">
							</div>
						</div>
						<!-- Divider -->
						<hr>
						<!-- Title -->
						<h6 class="card-title fw-normal"><a href="#">آموزش ساخت سایت خبری با لاراول</a></h6>
						<!-- Badge and Price -->
						<div class="d-flex justify-content-between align-items-center mb-0">
							<div><a href="#" class="badge bg-info bg-opacity-10 text-info me-2"><i class="fas fa-circle small fw-bold"></i> برنامه نویسی </a></div>
							<!-- Price -->
							<h5 class="text-success mb-0">140 تومان</h5>
						</div>
					</div>
				</div>
			</div>	
			<!-- Card Item END -->

			<!-- Card Item START -->
			<div class="col-md-6 col-lg-4 col-xxl-3">
				<div class="card p-2 shadow h-100">
					<div class="rounded-top overflow-hidden">
						<div class="card-overlay-hover">
							<!-- Image -->
							<img src="assets/images/courses/4by3/18.jpg" class="card-img-top" alt="course image">
						</div>
						<!-- Hover element -->
						<div class="card-img-overlay">
							<div class="card-element-hover d-flex justify-content-end">
								<a href="#" class="icon-md bg-white rounded-circle">
									<i class="fas fa-shopping-cart text-danger"></i>
								</a>
							</div>
						</div>
					</div>
					<!-- Card body -->
					<div class="card-body px-2">
						<!-- Badge and icon -->
						<div class="d-flex justify-content-between">
							<!-- Rating and info -->
							<ul class="list-inline hstack gap-2 mb-0">
								<!-- Info -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-orange bg-opacity-10 text-orange rounded-circle"><i class="fas fa-user-graduate"></i></div>
									<span class="h6 fw-light mb-0 ms-2">2.5k</span>
								</li>
								<!-- Rating -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-warning bg-opacity-15 text-warning rounded-circle"><i class="fas fa-star"></i></div>
									<span class="h6 fw-light mb-0 ms-2">3.6</span>
								</li>
							</ul>
							<!-- Avatar -->
							<div class="avatar avatar-sm">
								<img class="avatar-img rounded-circle" src="assets/images/avatar/07.jpg" alt="avatar">
							</div>
						</div>
						<!-- Divider -->
						<hr>
						<!-- Title -->
						<h6 class="card-title fw-normal"><a href="#">آموزش مقدماتی کتابخانه Pygame</a></h6>
						<!-- Badge and Price -->
						<div class="d-flex justify-content-between align-items-center mb-0">
							<div><a href="#" class="badge bg-info bg-opacity-10 text-info me-2"><i class="fas fa-circle small fw-bold"></i> استارت آپ </a></div>
							<!-- Price -->
							<h5 class="text-success mb-0">160 تومان</h5>
						</div>
					</div>
				</div>
			</div>
			<!-- Card Item END -->

			<!-- Card Item START -->
			<div class="col-md-6 col-lg-4 col-xxl-3">
				<div class="card p-2 shadow h-100">
					<div class="rounded-top overflow-hidden">
						<div class="card-overlay-hover">
							<!-- Image -->
							<img src="assets/images/courses/4by3/21.jpg" class="card-img-top" alt="course image">
						</div>
						<!-- Hover element -->
						<div class="card-img-overlay">
							<div class="card-element-hover d-flex justify-content-end">
								<a href="#" class="icon-md bg-white rounded-circle">
									<i class="fas fa-shopping-cart text-danger"></i>
								</a>
							</div>
						</div>
					</div>
					<!-- Card body -->
					<div class="card-body px-2">
						<!-- Badge and icon -->
						<div class="d-flex justify-content-between">
							<!-- Rating and info -->
							<ul class="list-inline hstack gap-2 mb-0">
								<!-- Info -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-orange bg-opacity-10 text-orange rounded-circle"><i class="fas fa-user-graduate"></i></div>
									<span class="h6 fw-light mb-0 ms-2">6k</span>
								</li>
								<!-- Rating -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-warning bg-opacity-15 text-warning rounded-circle"><i class="fas fa-star"></i></div>
									<span class="h6 fw-light mb-0 ms-2">3.8</span>
								</li>
							</ul>
							<!-- Avatar -->
							<div class="avatar avatar-sm">
								<img class="avatar-img rounded-circle" src="assets/images/avatar/05.jpg" alt="avatar">
							</div>
						</div>
						<!-- Divider -->
						<hr>
						<!-- Title -->
						<h6 class="card-title fw-normal"><a href="#">آموزش ساخت شبکه اجتماعی با MERN</a></h6>
						<!-- Badge and Price -->
						<div class="d-flex justify-content-between align-items-center mb-0">
							<div><a href="#" class="badge bg-info bg-opacity-10 text-info me-2"><i class="fas fa-circle small fw-bold"></i> بهینه سازی </a></div>
							<!-- Price -->
							<h5 class="text-success mb-0">226 تومان</h5>
						</div>
					</div>
				</div>
			</div>
			<!-- Card Item END -->

			<!-- Card Item START -->
			<div class="col-md-6 col-lg-4 col-xxl-3">
				<div class="card p-2 shadow h-100">
					<div class="rounded-top overflow-hidden">
						<div class="card-overlay-hover">
							<!-- Image -->
							<img src="assets/images/courses/4by3/20.jpg" class="card-img-top" alt="course image">
						</div>
						<!-- Hover element -->
						<div class="card-img-overlay">
							<div class="card-element-hover d-flex justify-content-end">
								<a href="#" class="icon-md bg-white rounded-circle">
									<i class="fas fa-shopping-cart text-danger"></i>
								</a>
							</div>
						</div>
					</div>
					<!-- Card body -->
					<div class="card-body px-2">
						<!-- Badge and icon -->
						<div class="d-flex justify-content-between">
							<!-- Rating and info -->
							<ul class="list-inline hstack gap-2 mb-0">
								<!-- Info -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-orange bg-opacity-10 text-orange rounded-circle"><i class="fas fa-user-graduate"></i></div>
									<span class="h6 fw-light mb-0 ms-2">15k</span>
								</li>
								<!-- Rating -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-warning bg-opacity-15 text-warning rounded-circle"><i class="fas fa-star"></i></div>
									<span class="h6 fw-light mb-0 ms-2">4.8</span>
								</li>
							</ul>
							<!-- Avatar -->
							<div class="avatar avatar-sm">
								<img class="avatar-img rounded-circle" src="assets/images/avatar/02.jpg" alt="avatar">
							</div>
						</div>
						<!-- Divider -->
						<hr>
						<!-- Title -->
						<h6 class="card-title fw-normal"><a href="#">دوره بازاریابی دیجیتال برای مبتدیان</a></h6>
						<!-- Badge and Price -->
						<div class="d-flex justify-content-between align-items-center mb-0">
							<div><a href="#" class="badge bg-info bg-opacity-10 text-info me-2"><i class="fas fa-circle small fw-bold"></i> طراحی وب </a></div>
							<!-- Price -->
							<h5 class="text-success mb-0">342 تومان</h5>
						</div>
					</div>
				</div>
			</div>
			<!-- Card Item END -->

			<!-- Card Item START -->
			<div class="col-md-6 col-lg-4 col-xxl-3">
				<div class="card p-2 shadow h-100">
					<div class="rounded-top overflow-hidden">
						<div class="card-overlay-hover">
							<!-- Image -->
							<img src="assets/images/courses/4by3/15.jpg" class="card-img-top" alt="course image">
						</div>
						<!-- Hover element -->
						<div class="card-img-overlay">
							<div class="card-element-hover d-flex justify-content-end">
								<a href="#" class="icon-md bg-white rounded-circle">
									<i class="fas fa-shopping-cart text-danger"></i>
								</a>
							</div>
						</div>
					</div>
					<!-- Card body -->
					<div class="card-body px-2">
						<!-- Badge and icon -->
						<div class="d-flex justify-content-between">
							<!-- Rating and info -->
							<ul class="list-inline hstack gap-2 mb-0">
								<!-- Info -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-orange bg-opacity-10 text-orange rounded-circle"><i class="fas fa-user-graduate"></i></div>
									<span class="h6 fw-light mb-0 ms-2">8k</span>
								</li>
								<!-- Rating -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-warning bg-opacity-15 text-warning rounded-circle"><i class="fas fa-star"></i></div>
									<span class="h6 fw-light mb-0 ms-2">3.6</span>
								</li>
							</ul>
							<!-- Avatar -->
							<div class="avatar avatar-sm">
								<img class="avatar-img rounded-circle" src="assets/images/avatar/11.jpg" alt="avatar">
							</div>
						</div>
						<!-- Divider -->
						<hr>
						<!-- Title -->
						<h6 class="card-title fw-normal"><a href="#">آموزش رایگان مایکروسافت پروجکت MSP</a></h6>
						<!-- Badge and Price -->
						<div class="d-flex justify-content-between align-items-center mb-0">
							<div><a href="#" class="badge bg-info bg-opacity-10 text-info me-2"><i class="fas fa-circle small fw-bold"></i> تکنولوژی </a></div>
							<!-- Price -->
							<h5 class="text-success mb-0">245 تومان</h5>
						</div>
					</div>
				</div>
			</div>
			<!-- Card Item END -->

			<!-- Card Item START -->
			<div class="col-md-6 col-lg-4 col-xxl-3">
				<div class="card p-2 shadow h-100">
					<div class="rounded-top overflow-hidden">
						<div class="card-overlay-hover">
							<!-- Image -->
							<img src="assets/images/courses/4by3/14.jpg" class="card-img-top" alt="course image">
						</div>
						<!-- Hover element -->
						<div class="card-img-overlay">
							<div class="card-element-hover d-flex justify-content-end">
								<a href="#" class="icon-md bg-white rounded-circle">
									<i class="fas fa-shopping-cart text-danger"></i>
								</a>
							</div>
						</div>
					</div>
					<!-- Card body -->
					<div class="card-body px-2">
						<!-- Badge and icon -->
						<div class="d-flex justify-content-between">
							<!-- Rating and info -->
							<ul class="list-inline hstack gap-2 mb-0">
								<!-- Info -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-orange bg-opacity-10 text-orange rounded-circle"><i class="fas fa-user-graduate"></i></div>
									<span class="h6 fw-light mb-0 ms-2">4k</span>
								</li>
								<!-- Rating -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-warning bg-opacity-15 text-warning rounded-circle"><i class="fas fa-star"></i></div>
									<span class="h6 fw-light mb-0 ms-2">4.0</span>
								</li>
							</ul>
							<!-- Avatar -->
							<div class="avatar avatar-sm">
								<img class="avatar-img rounded-circle" src="assets/images/avatar/12.jpg" alt="avatar">
							</div>
						</div>
						<!-- Divider -->
						<hr>
						<!-- Title -->
						<h6 class="card-title fw-normal"><a href="#">طراحی های خلاقانه رابط کاربری وب</a></h6>
						<!-- Badge and Price -->
						<div class="d-flex justify-content-between align-items-center mb-0">
							<div><a href="#" class="badge bg-info bg-opacity-10 text-info me-2"><i class="fas fa-circle small fw-bold"></i> برنامه نویسی موبایل </a></div>
							<!-- Price -->
							<h5 class="text-success mb-0">199 تومان</h5>
						</div>
					</div>
				</div>
			</div>
			<!-- Card Item END -->

			<!-- Card Item START -->
			<div class="col-md-6 col-lg-4 col-xxl-3">
				<div class="card p-2 shadow h-100">
					<div class="rounded-top overflow-hidden">
						<div class="card-overlay-hover">
							<!-- Image -->
							<img src="assets/images/courses/4by3/19.jpg" class="card-img-top" alt="course image">
						</div>
						<!-- Hover element -->
						<div class="card-img-overlay">
							<div class="card-element-hover d-flex justify-content-end">
								<a href="#" class="icon-md bg-white rounded-circle">
									<i class="fas fa-shopping-cart text-danger"></i>
								</a>
							</div>
						</div>
					</div>
					<!-- Card body -->
					<div class="card-body px-2">
						<!-- Badge and icon -->
						<div class="d-flex justify-content-between">
							<!-- Rating and info -->
							<ul class="list-inline hstack gap-2 mb-0">
								<!-- Info -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-orange bg-opacity-10 text-orange rounded-circle"><i class="fas fa-user-graduate"></i></div>
									<span class="h6 fw-light mb-0 ms-2">6k</span>
								</li>
								<!-- Rating -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-warning bg-opacity-15 text-warning rounded-circle"><i class="fas fa-star"></i></div>
									<span class="h6 fw-light mb-0 ms-2">4.0</span>
								</li>
							</ul>
							<!-- Avatar -->
							<div class="avatar avatar-sm">
								<img class="avatar-img rounded-circle" src="assets/images/avatar/08.jpg" alt="avatar">
							</div>
						</div>
						<!-- Divider -->
						<hr>
						<!-- Title -->
						<h6 class="card-title fw-normal"><a href="#"> معماری میکروفرانت اند ها در React </a></h6>
						<!-- Badge and Price -->
						<div class="d-flex justify-content-between align-items-center mb-0">
							<div><a href="#" class="badge bg-info bg-opacity-10 text-info me-2"><i class="fas fa-circle small fw-bold"></i> بانک اطلاعات</a></div>
							<!-- Price -->
							<h5 class="text-success mb-0">215 تومان</h5>
						</div>
					</div>
				</div>
			</div>
			<!-- Card Item END -->

			<!-- Card Item START -->
			<div class="col-md-6 col-lg-4 col-xxl-3">
				<div class="card p-2 shadow h-100">
					<div class="rounded-top overflow-hidden">
						<div class="card-overlay-hover">
							<!-- Image -->
							<img src="assets/images/courses/4by3/16.jpg" class="card-img-top" alt="course image">
						</div>
						<!-- Hover element -->
						<div class="card-img-overlay">
							<div class="card-element-hover d-flex justify-content-end">
								<a href="#" class="icon-md bg-white rounded-circle">
									<i class="fas fa-shopping-cart text-danger"></i>
								</a>
							</div>
						</div>
					</div>
					<!-- Card body -->
					<div class="card-body px-2">
						<!-- Badge and icon -->
						<div class="d-flex justify-content-between">
							<!-- Rating and info -->
							<ul class="list-inline hstack gap-2 mb-0">
								<!-- Info -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-orange bg-opacity-10 text-orange rounded-circle"><i class="fas fa-user-graduate"></i></div>
									<span class="h6 fw-light mb-0 ms-2">2k</span>
								</li>
								<!-- Rating -->
								<li class="list-inline-item d-flex justify-content-center align-items-center">
									<div class="icon-md bg-warning bg-opacity-15 text-warning rounded-circle"><i class="fas fa-star"></i></div>
									<span class="h6 fw-light mb-0 ms-2">3.5</span>
								</li>
							</ul>
							<!-- Avatar -->
							<div class="avatar avatar-sm">
								<img class="avatar-img rounded-circle" src="assets/images/avatar/06.jpg" alt="avatar">
							</div>
						</div>
						<!-- Divider -->
						<hr>
						<!-- Title -->
						<h6 class="card-title fw-normal"><a href="#">آموزش کاربردی گیت و گیت هاب</a></h6>
						<!-- Badge and Price -->
						<div class="d-flex justify-content-between align-items-center mb-0">
							<div><a href="#" class="badge bg-info bg-opacity-10 text-info me-2"><i class="fas fa-circle small fw-bold"></i> مهندسی نرم افزار</a></div>
							<!-- Price -->
							<h5 class="text-success mb-0">112 تومان</h5>
						</div>
					</div>
				</div>
			</div>
			<!-- Card Item END -->

		</div>
		<!-- Button -->
		<div class="text-center mt-5">
			<a href="#" class="btn btn-primary-soft">مشاهده همه<i class="fas fa-sync ms-2"></i></a>
		</div>
	</div>
</section>
<!-- =======================
Featured course END -->
<!--Action box START -->
<section class="py-0">
	<div class="container">
		<div class="row g-4">
			<!-- Action box item -->
			<div class="col-lg-6 position-relative overflow-hidden">
				<div class="bg-primary bg-opacity-10 rounded-3 p-5 h-100">
					<!-- Image -->
					<div class="position-absolute bottom-0 end-0 me-3">
						<img src="assets/images/element/08.svg" class="h-100px h-sm-200px" alt="">
					</div>	
					<!-- Content -->
					<div class="row">
						<div class="col-sm-8 position-relative">
							<h2 class="mb-1 h4">اعطای مدرک معتبر</h2>
							<p class="mb-3 h5 fw-light">برنامه گواهینامه حرفه ای مناسب را برای خود دریافت کنید.</p>
							<a href="#" class="btn btn-primary mb-0">مشاهده</a>
						</div>
					</div>
				</div>
			</div>

			<!-- Action box item -->
			<div class="col-lg-6 position-relative overflow-hidden">
				<div class="bg-secondary rounded-3 bg-opacity-10 p-5 h-100">
					<!-- Image -->
					<div class="position-absolute bottom-0 end-0 me-3">
						<img src="assets/images/element/15.svg" class="h-100px h-sm-200px" alt="">
					</div>	
					<!-- Content -->
					<div class="row">
						<div class="col-sm-8 position-relative">
							<h2 class="mb-1 h4">بهترین دوره های آنلاین</h2>
							<p class="mb-3 h5 fw-light">اکنون در محبوب ترین و بهترین دوره ها ثبت نام کنید.</p>
							<a href="#" class="btn btn-warning mb-0">مشاهده</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- =======================
Action box END -->


<!-- =======================
Gallery START-->
<section class="pt-0 pt-md-5">
	<div class="container">
		<!-- Title -->
		<div class="row mb-3 mb-sm-4">
			<div class="col-12 mx-auto text-center">
				<h2 class="fs-1 fw-bold fs-3">
					<span class="position-relative z-index-9">گالـری </span>
					<span class="position-relative z-index-1">تصاویر
						<!-- SVG START -->
						<span class="position-absolute top-50 start-50 translate-middle z-index-n1 d-none d-sm-block">
							<svg style="transform: scale(-1,1)" class="fill-orange" width="150" height="86" viewBox="0 0 303 86">
								<path d="M288.197 19.3999C281.599 15.6998 273.654 13.03 265.424 10.926C256.533 8.64794 247.263 6.92124 237.946 5.4267C218.461 2.249 198.384 0.406219 178.237 0.0579769C158.609 -0.275755 138.888 0.8125 119.733 3.49686C108.17 5.10748 96.8189 7.2985 85.8466 10.0264C81.4955 11.0131 77.168 12.0723 72.9115 13.2331C56.382 17.7022 40.5146 23.4192 26.3972 30.355C12.9182 36.9861 0.716203 46.0404 0.999971 57.2131C1.14185 62.2772 4.16871 67.051 9.98595 70.693C15.4721 74.1319 22.6846 76.3809 29.9679 78.0206C38.7647 80.0085 48.0345 81.3289 57.257 82.4026C67.1179 83.5489 77.0734 84.2889 87.0762 84.6807C107.413 85.4642 127.892 84.7968 148.063 83.0266C168.399 81.2418 188.429 78.3543 208.127 74.8139C227.399 71.3459 246.436 67.2976 265.141 62.8285C278.927 59.5348 294.227 55.0802 300.446 46.2435C307.091 36.812 299.949 25.973 288.197 19.3999ZM298.862 46.7804C295.48 50.9593 289.592 54.0935 283.207 56.4876C276.633 58.9543 269.468 60.7391 262.279 62.4077C252.915 64.5843 243.503 66.6737 234.044 68.6616C215.174 72.6083 196.019 76.0762 176.534 78.7171C157.191 81.3289 137.54 83.0991 117.747 83.6505C97.9304 84.2019 77.9957 83.5634 58.4866 81.3289C49.5243 80.2987 40.5146 79.0363 31.907 77.1645C24.5764 75.5829 17.3403 73.4499 11.6649 70.1126C5.49296 66.4561 2.15869 61.5226 2.22963 56.2555C2.32422 50.7417 5.72943 45.489 10.9555 41.0489C16.1106 36.6959 22.7319 33.0974 29.6842 29.8472C36.2108 26.8145 43.0213 24.0141 50.0918 21.4748C48.4601 22.1278 46.8521 22.7953 45.2678 23.4772C37.7716 26.684 30.4409 30.1664 23.9615 34.1131C17.695 37.9438 12.1615 42.3839 9.30018 47.5785C6.55709 52.5554 6.10779 58.1853 9.77313 63.0462C13.0838 67.4427 19.303 70.7655 26.279 72.8985C34.6974 75.4813 44.2036 76.2358 53.497 76.381C63.8309 76.5406 74.2357 76.1488 84.5696 75.757C95.0454 75.3652 105.497 74.7993 115.926 74.0884C136.783 72.6664 157.545 70.6204 178.071 67.9361C187.956 66.6447 197.817 65.2227 207.583 63.6411C208.269 63.525 208.718 62.3642 208.009 62.4658C188.358 65.629 168.447 68.2118 148.394 70.2142C128.435 72.202 108.312 73.624 88.1404 74.4366C78.0666 74.8429 67.9219 75.1911 57.8008 75.2056C48.3419 75.2201 38.6465 74.7558 29.8261 72.5068C22.5427 70.6785 15.8032 67.6169 11.8777 63.3509C7.33745 58.4175 7.52663 52.4393 10.6481 47.2302C13.7695 41.9776 19.7523 37.581 26.3263 33.8084C32.7583 30.1083 39.8289 26.7855 47.1359 23.7529C59.2197 18.7034 72.2257 14.4955 85.7756 11.1292C90.7889 9.99737 95.8494 8.98167 100.981 8.08205C117.96 5.07846 135.553 3.32274 153.218 2.88744C161.754 2.66979 170.315 2.78587 178.851 3.19215C179.537 3.22117 180.128 2.06037 179.277 2.01684C167.69 1.45094 156.032 1.47996 144.468 2.06037C145.745 1.97331 146.999 1.88625 148.275 1.8137C167.879 0.6674 187.696 1.04466 207.157 2.78587C226.075 4.46904 245.111 7.25497 262.894 11.608C278.714 15.4677 294.085 21.6635 299.808 32.0092C302.456 36.812 302.574 42.1662 298.862 46.7804Z"/>
							</svg>
						</span>
						<!-- SVG END -->
					</span>
				</h2>
			</div>
		</div>

		<!-- Image gallery START -->
		<div class="row g-4">

			<div class="col-lg-4">
				<div class="row g-4">

					<!-- Image -->
					<div class="col-md-6">
						<div class="card overflow-hidden">
							<div class="card-overlay-hover">
								<!-- Image -->
								<img src="assets/images/event/11.jpg" class="rounded-3" alt="course image">
							</div>
							<!-- Full screen button -->
							<a class="card-element-hover position-absolute w-100 h-100" data-glightbox data-gallery="gallery" href="assets/images/event/11.jpg">
								<i class="bi bi-fullscreen fs-6 text-white position-absolute top-50 start-50 translate-middle bg-dark rounded-3 p-2 lh-1"></i>
							</a>
						</div>
					</div>

					<!-- Image -->
					<div class="col-md-6">
						<div class="card overflow-hidden">
							<div class="card-overlay-hover">
								<!-- Image -->
								<img src="assets/images/event/12.jpg" class="rounded-3" alt="course image">
							</div>
							<!-- Full screen button -->
							<a class="card-element-hover position-absolute w-100 h-100" data-glightbox data-gallery="gallery" href="assets/images/event/12.jpg">
								<i class="bi bi-fullscreen fs-6 text-white position-absolute top-50 start-50 translate-middle bg-dark rounded-3 p-2 lh-1"></i>
							</a>
						</div>
					</div>

					<!-- Image -->
					<div class="col-12">
						<div class="card overflow-hidden">
							<div class="card-overlay-hover">
								<!-- Image -->
								<img src="assets/images/event/14.jpg" class="rounded-3" alt="course image">
							</div>
							<!-- Full screen button -->
							<a class="card-element-hover position-absolute w-100 h-100" data-glightbox data-gallery="gallery" href="assets/images/event/14.jpg">
								<i class="bi bi-fullscreen fs-6 text-white position-absolute top-50 start-50 translate-middle bg-dark rounded-3 p-2 lh-1"></i>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="card overflow-hidden">
					<div class="card-overlay-hover">
						<!-- Image -->
						<img src="assets/images/event/17.jpg" class="rounded-3" alt="course image">
					</div>
					<!-- Full screen button -->
					<a class="card-element-hover position-absolute w-100 h-100" data-glightbox data-gallery="gallery" href="assets/images/event/17.jpg">
						<i class="bi bi-fullscreen fs-6 text-white position-absolute top-50 start-50 translate-middle bg-dark rounded-3 p-2 lh-1"></i>
					</a>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="row g-4">

					<!-- Video -->
					<div class="col-12">
						<div class="card overflow-hidden">
							<div class="card-overlay-hover">
								<!-- Image -->
								<img src="assets/images/event/16.jpg" class="rounded-3" alt="course image">
							</div>
							<!-- Full screen button -->
							<a class="card-element-hover position-absolute w-100 h-100" data-glightbox data-gallery="gallery" href="https://www.aparat.com/video/video/embed/videohash/31hor/vt/frame">
								<span class="btn text-danger btn-round btn-white-shadow mb-0 position-absolute top-50 start-50 translate-middle">
									<i class="fas fa-play"></i>
								</span>
							</a>
						</div>
					</div>

					<!-- Image -->
					<div class="col-md-6">
						<div class="card overflow-hidden">
							<div class="card-overlay-hover">
								<!-- Image -->
								<img src="assets/images/event/13.jpg" class="rounded-3" alt="course image">
							</div>
							<!-- Full screen button -->
							<a class="card-element-hover position-absolute w-100 h-100" data-glightbox data-gallery="gallery" href="assets/images/event/13.jpg">
								<i class="bi bi-fullscreen fs-6 text-white position-absolute top-50 start-50 translate-middle bg-dark rounded-3 p-2 lh-1"></i>
							</a>
						</div>
					</div>

					<!-- Image -->
					<div class="col-md-6">
						<div class="card overflow-hidden">
							<div class="card-overlay-hover">
								<!-- Image -->
								<img src="assets/images/event/15.jpg" class="rounded-3" alt="course image">
							</div>
							<!-- Full screen button -->
							<a class="card-element-hover position-absolute w-100 h-100" data-glightbox data-gallery="gallery" href="assets/images/event/15.jpg">
								<i class="bi bi-fullscreen fs-6 text-white position-absolute top-50 start-50 translate-middle bg-dark rounded-3 p-2 lh-1"></i>
							</a>
						</div>
					</div>

				</div> <!-- Row END -->
			</div>
		</div>
		<!-- Image gallery END -->
	</div>
</section>
<!-- =======================
Gallery START-->

<!-- =======================
Blog START-->
<section class="pt-0 pt-md-5">
	<div class="container">
		<!-- Title -->
		<div class="row mb-3 mb-sm-4">
			<div class="col-12 mx-auto text-center">
				<h2 class="h1 fs-3">
					<span class="position-relative z-index-9">جدیدترین</span>
					<span class="position-relative z-index-1"> اخبـار و وبلاگ
						<!-- SVG START -->
						<span class="position-absolute top-50 start-50 translate-middle z-index-n1 d-none d-sm-block">
							<svg style="transform: scale(-1,1)" class="fill-orange" width="150" height="86" viewBox="0 0 303 86">
								<path d="M288.197 19.3999C281.599 15.6998 273.654 13.03 265.424 10.926C256.533 8.64794 247.263 6.92124 237.946 5.4267C218.461 2.249 198.384 0.406219 178.237 0.0579769C158.609 -0.275755 138.888 0.8125 119.733 3.49686C108.17 5.10748 96.8189 7.2985 85.8466 10.0264C81.4955 11.0131 77.168 12.0723 72.9115 13.2331C56.382 17.7022 40.5146 23.4192 26.3972 30.355C12.9182 36.9861 0.716203 46.0404 0.999971 57.2131C1.14185 62.2772 4.16871 67.051 9.98595 70.693C15.4721 74.1319 22.6846 76.3809 29.9679 78.0206C38.7647 80.0085 48.0345 81.3289 57.257 82.4026C67.1179 83.5489 77.0734 84.2889 87.0762 84.6807C107.413 85.4642 127.892 84.7968 148.063 83.0266C168.399 81.2418 188.429 78.3543 208.127 74.8139C227.399 71.3459 246.436 67.2976 265.141 62.8285C278.927 59.5348 294.227 55.0802 300.446 46.2435C307.091 36.812 299.949 25.973 288.197 19.3999ZM298.862 46.7804C295.48 50.9593 289.592 54.0935 283.207 56.4876C276.633 58.9543 269.468 60.7391 262.279 62.4077C252.915 64.5843 243.503 66.6737 234.044 68.6616C215.174 72.6083 196.019 76.0762 176.534 78.7171C157.191 81.3289 137.54 83.0991 117.747 83.6505C97.9304 84.2019 77.9957 83.5634 58.4866 81.3289C49.5243 80.2987 40.5146 79.0363 31.907 77.1645C24.5764 75.5829 17.3403 73.4499 11.6649 70.1126C5.49296 66.4561 2.15869 61.5226 2.22963 56.2555C2.32422 50.7417 5.72943 45.489 10.9555 41.0489C16.1106 36.6959 22.7319 33.0974 29.6842 29.8472C36.2108 26.8145 43.0213 24.0141 50.0918 21.4748C48.4601 22.1278 46.8521 22.7953 45.2678 23.4772C37.7716 26.684 30.4409 30.1664 23.9615 34.1131C17.695 37.9438 12.1615 42.3839 9.30018 47.5785C6.55709 52.5554 6.10779 58.1853 9.77313 63.0462C13.0838 67.4427 19.303 70.7655 26.279 72.8985C34.6974 75.4813 44.2036 76.2358 53.497 76.381C63.8309 76.5406 74.2357 76.1488 84.5696 75.757C95.0454 75.3652 105.497 74.7993 115.926 74.0884C136.783 72.6664 157.545 70.6204 178.071 67.9361C187.956 66.6447 197.817 65.2227 207.583 63.6411C208.269 63.525 208.718 62.3642 208.009 62.4658C188.358 65.629 168.447 68.2118 148.394 70.2142C128.435 72.202 108.312 73.624 88.1404 74.4366C78.0666 74.8429 67.9219 75.1911 57.8008 75.2056C48.3419 75.2201 38.6465 74.7558 29.8261 72.5068C22.5427 70.6785 15.8032 67.6169 11.8777 63.3509C7.33745 58.4175 7.52663 52.4393 10.6481 47.2302C13.7695 41.9776 19.7523 37.581 26.3263 33.8084C32.7583 30.1083 39.8289 26.7855 47.1359 23.7529C59.2197 18.7034 72.2257 14.4955 85.7756 11.1292C90.7889 9.99737 95.8494 8.98167 100.981 8.08205C117.96 5.07846 135.553 3.32274 153.218 2.88744C161.754 2.66979 170.315 2.78587 178.851 3.19215C179.537 3.22117 180.128 2.06037 179.277 2.01684C167.69 1.45094 156.032 1.47996 144.468 2.06037C145.745 1.97331 146.999 1.88625 148.275 1.8137C167.879 0.6674 187.696 1.04466 207.157 2.78587C226.075 4.46904 245.111 7.25497 262.894 11.608C278.714 15.4677 294.085 21.6635 299.808 32.0092C302.456 36.812 302.574 42.1662 298.862 46.7804Z"/>
							</svg>
						</span>
						<!-- SVG END -->
					</span>
				</h2>
			</div>
		</div>

		<div class="row g-4 justify-content-between">

			<div class="col-md-5">
				<!-- Card START -->
				<div class="card bg-transparent">
					<!-- Image -->
					<img src="assets/images/event/18.jpg" class="card-img" alt="">

					<!-- Card body -->
					<div class="card-body px-3">
						<h4 class="fs-5 ff-vb"><a href="#" class="stretched-link">فریم ورک‌ها و کتابخانه‌های CSS برای پروژه</a></h4>
						<p>مان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.</p>
						<div class="d-flex justify-content-between">
							<h6>سارا حسینی</h6>
							<span>29 مرداد 1400</span>
						</div>
					</div>
				</div>
				<!-- Card END -->
			</div>

			<div class="col-md-7">
				<!-- Card START -->
				<div class="card bg-transparent">
					<div class="row align-items-center">
						<div class="col-sm-4 col-lg-3">
							<div class="bg-dark text-center p-3 rounded-2 mb-2 mb-sm-0">
								<h2 class="text-white">15</h2>
								<span class="text-white">تیر 1400</span>
							</div>
						</div>
						<div class="col-sm-8 col-lg-9">
							<div class="card-body p-0">
								<h5 class="card-title"><a href="#" class="stretched-link">چطور یک پروژه برنامه نویسی را مرحله به مرحله بسازیم؟</a></h5>
								<p class="mb-0">ناصر احمدی</p>
							</div>
						</div>
					</div>
				</div>
				<!-- Card END -->

				<hr class="my-4">

				<!-- Card START -->
				<div class="card bg-transparent">
					<div class="row align-items-center">
						<div class="col-sm-4 col-lg-3">
							<div class="bg-dark text-center p-3 rounded-2 mb-2 mb-sm-0">
								<h2 class="text-white">12</h2>
								<span class="text-white">خرداد 1400</span>
							</div>
						</div>
						<div class="col-sm-8 col-lg-9">
							<div class="card-body p-0">
								<h5 class="card-title"><a href="#" class="stretched-link">افراد مسلط به اکسل ، چه درآمدی دارند و کجا استخدام می شوند؟</a></h5>
								<p class="mb-0">میثم قربانی</p>
							</div>
						</div>
					</div>
				</div>
				<!-- Card END -->

				<hr class="my-4">

				<!-- Card START -->
				<div class="card bg-transparent">
					<div class="row align-items-center">
						<div class="col-sm-4 col-lg-3">
							<div class="bg-dark text-center p-3 rounded-2 mb-2 mb-sm-0">
								<h2 class="text-white">09</h2>
								<span class="text-white">فروردین</span>
							</div>
						</div>
						<div class="col-sm-8 col-lg-9">
							<div class="card-body p-0">
								<h5 class="card-title"><a href="#" class="stretched-link"> قدرتمندترین مزایای استفاده از AngularJS برای توسعه برنامه‌های وب</a></h5>
								<p class="mb-0">قاسم اصلانی</p>
							</div>
						</div>
					</div>
				</div>
				<!-- Card END -->
			</div>
		</div> <!-- Row END -->
	</div>
</section>
<!-- =======================
Blog START-->

</main>
@stop


