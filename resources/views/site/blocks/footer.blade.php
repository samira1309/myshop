<footer class="footer position-relativ">
	<div class="container">
		<div class="top">
			<div class="row w-100 m-0">
				<div class="col-xxl-7 col-xl-7 col-md-6 col-sm-12 p-0">
					<div class="row w-100 m-0">
						<div class="col-xxl-3 col-xl-3 col-md-12 col-sm-3 col-xs-12 p-md-2 p-sm-0 p-xs-0">
							<a href="tel:{{@$setting_header->contact}}">
								<div class="row w-100 m-0">
									<div
										class="col-xl-3 col-md-2 col-sm-3 col-xs-2 align-self-center text-center p-1">
										<svg xmlns="http://www.w3.org/2000/svg"  fill="currentColor" class="bi bi-telephone" viewBox="0 0 16 16">
  <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
</svg>
									</div>
									<div class="col-xl-9 col-md-10 col-sm-9 col-xs-10 align-self-center p-1">
										<p class="text-one h2">
											تماس با پشتیبانی
										</p>
										<p class="text-white h4">
											{{@$setting_header->contact}}
										</p>
									</div>
								</div>
							</a>
						</div>
						<div class="col-xxl-9 col-xl-9 col-md-12 col-sm-9 col-xs-12 p-md-2 p-sm-0 p-xs-0">
							<a href="#">
								<div class="row w-100 m-0">
									<div
										class="col-xl-2 col-md-2 col-sm-2 col-xs-2 align-self-center text-center p-1">
										<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16">
  <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
  <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
</svg>
									</div>
									<div class="col-xl-10 col-md-10 col-sm-10 col-xs-10 align-self-center p-1">
										<p class="text-one h2">
											آدرس فروشگاه مرکزی
										</p>
										<p class="text-white h4">
											{{@$setting_header->address}}
										</p>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
				<div class="col-xxl-5 col-xl-5 col-md-6 col-sm-12 p-0">
					<div class="row w-100 m-0">
						<div class="col-xl-6 col-md-12 col-sm-6 col-xs-12 p-md-2 p-sm-0 p-xs-0">
							<a href="mailto:{{@$setting_header->email}}">
								<div class="row w-100 m-0">
									<div
										class="col-xl-3 col-md-2 col-sm-2 col-xs-2 align-self-center text-center p-1">
									<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-envelope" viewBox="0 0 16 16">
  <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4Zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2Zm13 2.383-4.708 2.825L15 11.105V5.383Zm-.034 6.876-5.64-3.471L8 9.583l-1.326-.795-5.64 3.47A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.741ZM1 11.105l4.708-2.897L1 5.383v5.722Z"/>
</svg>
									</div>
									<div class="col-xl-9 col-md-10 col-sm-10 col-xs-10 align-self-center p-1">
										<p class="text-one h2">
											ایمیل پشتیبانی
										</p>
										<p class="text-white h4">
											{{@$setting_header->email}}
										</p>
									</div>
								</div>
							</a>
						</div>
						<div class="col-xl-6 col-md-12 d-md-block d-sm-none d-xs-none p-md-2 p-sm-0 p-xs-0">
							<a href="tel:{{@$setting_header->phone}}">
								<div class="row w-100 m-0">
									<div
										class="col-xl-3 col-md-2 col-sm-2 col-xs-2 align-self-center text-center p-1">
										<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-headset" viewBox="0 0 16 16">
  <path d="M8 1a5 5 0 0 0-5 5v1h1a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V6a6 6 0 1 1 12 0v6a2.5 2.5 0 0 1-2.5 2.5H9.366a1 1 0 0 1-.866.5h-1a1 1 0 1 1 0-2h1a1 1 0 0 1 .866.5H11.5A1.5 1.5 0 0 0 13 12h-1a1 1 0 0 1-1-1V8a1 1 0 0 1 1-1h1V6a5 5 0 0 0-5-5z"/>
</svg>
									</div>
									<div class="col-xl-9 col-md-10 col-sm-10 col-xs-10 align-self-center p-1">
										<p class="text-one h2">
										مشاوره
										</p>
										<p class="text-white h4">
											{{@$setting_header->phone}}
										</p>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<hr class="my-xs-1 my-sm-2">
		<div class="d-md-block d-sm-none d-xs-none">
			<div class="bottom">
				<div class="row w-100 m-0">
					<div class="col-md-7 col-sm-12 p-0">
						<div class="row w-100 m-0">
							<div class="col-sm-4 col-xs-5 mx-auto p-2">
								<p class="fw-bolder h4 text-white">
									دسترسی سریع
								</p>
								<ul class="p-3">
									<li class="text-one">
										<a href="{{route('site.timer')}}" class="text-white">
											تخفیف دارها
										</a>
									</li>
									<li class="text-one">
										<a href="{{route('panel.log')}}" class="text-white">
											ورود
										</a>
									</li>
									<li class="text-one">
										<a href="{{route('panel.register')}}" class="text-white">
											ثبت نام
										</a>
									</li>
									<li class="text-one">
										<a href="{{route('site.rules')}}" class="text-white">
											قوانین و مقررات
										</a>
									</li>
									<li class="text-one">
										<a href="{{route('site.pay')}}" class="text-white">
											راهنمای خرید
										</a>
									</li>
										<li class="text-one">
										<a href="{{route('site.faq')}}" class="text-white">
سوالات متداول
										</a>
									</li>
									<li class="text-one">
										<a href="{{route('site.tracking')}}" class="text-white">
											پیگیری مرسوله پستی
										</a>
									</li>
									<li class="text-one">
										<a href="{{route('site.about')}}" class="text-white">
											درباره ما
										</a>
									</li>
									<li class="text-one">
										<a href="{{route('site.contact')}}" class="text-white">
											تماس با ما
										</a>
									</li>
								</ul>
							</div>
							<div class="col-sm-4 col-xs-5 mx-auto p-2">
								<p class="fw-bolder h4 text-white">
									دسته بندی محصولات
								</p>
								<ul class="p-3">
									@foreach($category_footer as $cat)
									<li class="text-one">
										<a href="{{route('site.product.list',['id'=>$cat->id])}}"
											class="text-white">
											{{@$cat->title}}
										</a>
									</li>
									@endforeach
								</ul>
							</div>
							<div class="col-sm-4 col-xs-6 mx-auto d-sm-block d-xs-none p-2">
								<p class="fw-bolder h4 text-white">
									برندهای پرطرفدار
								</p>
								<ul class="p-3">
									@foreach($brands_footer as $br)
									<li class="text-one">
										<a href="{{route('site.brand.detail',['id'=>$br->id])}}"
											class="text-white">
											{{@$br->title}}
										</a>
									</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-5 col-sm-12 p-2">
						<ul class="p-0 m-0 namad">
							<li class="">
							    <a referrerpolicy="origin" target="_blank" href="">
							        <img referrerpolicy="origin" src="{{asset('assets/site/images/kaj/namad.jpg')}}" 
							        width="100%" height="100%" style="cursor:pointer" id="eF5odJmk8n3NmaA3r0et"></a>
								<!--<a href="">-->
								<!--	<img src="{{asset('assets/site/images/kaj/psr.jpg')}}" alt="">-->
								<!--</a>-->
								<!--<a referrerpolicy="origin" target="_blank" href="https://trustseal.enamad.ir/?id=237212&amp;Code=eF5odJmk8n3NmaA3r0et">-->
								<!--    <img referrerpolicy="origin" src="{{asset('assets/site/images/kaj/nmd.jpg')}}" alt="" style="cursor:pointer" id="eF5odJmk8n3NmaA3r0et">-->
							 <!--   </a>-->
							</li>
							<!--<li class="">-->
							<!--	<a href="">-->
							<!--		<img src="{{asset('assets/site/images/kaj/nmd.jpg')}}" alt="">-->
							<!--	</a>-->
							<!--</li>-->
							<!--<li class="">-->
							<!--	<a href="">-->
							<!--		<img src="{{asset('assets/site/images/kaj/ath.jpg')}}" alt="">-->
							<!--	</a>-->
							<!--</li>-->
						</ul>
						<div class="description">
							<p>
								{{@$setting_header->about2}}
							</p>
						</div>
						<ul class="p-0 mx-auto my-4 d-flex max-content">
						
							<li class="list-unstyled">
								<a href="https://www.instagram.com/" class="bg-white d-flex mx-1 p-2 rounded-circle"
									rel="noopener noreferrer nofollow">
									<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-instagram text-one" viewBox="0 0 16 16">
  <path d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z"/>
</svg>
								</a>
							</li>
					        <li class="list-unstyled">
								<a href="https://t.me/" class="bg-white d-flex mx-1 p-2 rounded-circle"
									rel="noopener noreferrer nofollow">
									<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-telegram" viewBox="0 0 16 16">
  <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.287 5.906c-.778.324-2.334.994-4.666 2.01-.378.15-.577.298-.595.442-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294.26.006.549-.1.868-.32 2.179-1.471 3.304-2.214 3.374-2.23.05-.012.12-.026.166.016.047.041.042.12.037.141-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8.154 8.154 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629.093.06.183.125.27.187.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.426 1.426 0 0 0-.013-.315.337.337 0 0 0-.114-.217.526.526 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09z"/>
</svg>
</svg>
								</a>
							</li>
						</ul>
					</div>
				
				</div>
			</div>
		</div>
		<div class="d-md-none d-sm-block d-xs-block">
			<div class="bottom pb-5">
				<div class="accordion pb-5" id="accordionFooter">
					<div class="accordion-item">
						<p class="accordion-header h2" id="heading1">
							<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
								data-bs-target="#collapsefooter1" aria-expanded="false"
								aria-controls="collapsefooter1">
								دسترسی سریع
							</button>
						</p>
						<div id="collapsefooter1" class="accordion-collapse collapse" aria-labelledby="heading1"
							data-bs-parent="#accordionFooter">
							<div class="accordion-body">
								<ul class="px-3 py-0 m-0">
									<li class="text-one">
										<a href="{{route('site.timer')}}" class="text-white">
											تخفیف دارها
										</a>
									</li>
									<li class="text-one">
										<a href="{{route('panel.log')}}" class="text-white">
											ورود
										</a>
									</li>
									<li class="text-one">
										<a href="{{route('panel.register')}}" class="text-white">
											ثبت نام
										</a>
									</li>
									<li class="text-one">
										<a href="{{route('site.rules')}}" class="text-white">
											قوانین و مقررات
										</a>
									</li>
									<li class="text-one">
										<a href="{{route('site.pay')}}" class="text-white">
											راهنمای خرید
										</a>
									</li>
										<li class="text-one">
										<a href="{{route('site.faq')}}" class="text-white">
									سوالات متداول
										</a>
									</li>
									<li class="text-one">
										<a href="{{route('site.tracking')}}" class="text-white">
											پیگیری مرسوله پستی
										</a>
									</li>
									<li class="text-one">
										<a href="{{route('site.about')}}" class="text-white">
											درباره ما
										</a>
									</li>
									<li class="text-one">
										<a href="{{route('site.contact')}}" class="text-white">
											تماس با ما
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="accordion-item">
						<p class="accordion-header h2" id="heading2">
							<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
								data-bs-target="#collapsefooter2" aria-expanded="false"
								aria-controls="collapsefooter2">
								دسته بندی محصولات
							</button>
						</p>
						<div id="collapsefooter2" class="accordion-collapse collapse" aria-labelledby="heading2"
							data-bs-parent="#accordionFooter">
							<div class="accordion-body">
								<ul class="px-3 py-0 m-0">
									@foreach($category_footer as $cat)
									<li class="text-one">
										<a href="{{route('site.product.list',['id'=>$cat->id])}}"
											class="text-white">
											{{@$cat->title}}
										</a>
									</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
					<div class="accordion-item">
						<p class="accordion-header h2" id="heading3">
							<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
								data-bs-target="#collapsefooter3" aria-expanded="false"
								aria-controls="collapsefooter3">
								برندهای پرطرفدار
							</button>
						</p>
						<div id="collapsefooter3" class="accordion-collapse collapse" aria-labelledby="heading3"
							data-bs-parent="#accordionFooter">
							<div class="accordion-body">
								<ul class="px-3 py-0 m-0">
									@foreach($brands_footer as $br)
									<li class="text-one">
										<a href="{{route('site.brand.detail',['id'=>$br->id])}}"
											class="text-white">
											{{@$br->title}}
										</a>
									</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
					<div class="accordion-item">
						<p class="accordion-header h2" id="heading5">
							<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
								data-bs-target="#collapsefooter5" aria-expanded="false"
								aria-controls="collapsefooter5">
							درباره شرکت مبتکر
							</button>
						</p>
						<div id="collapsefooter5" class="accordion-collapse collapse" aria-labelledby="heading5"
							data-bs-parent="#accordionFooter">
							<div class="accordion-body">
								<div class="description">
									<p>
										{{@$setting_header->about2}}
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="accordion-item">
						<p class="accordion-header h2" id="heading4">
							<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
								data-bs-target="#collapsefooter4" aria-expanded="false"
								aria-controls="collapsefooter4">
								مجوزها
							</button>
						</p>
						<div id="collapsefooter4" class="accordion-collapse collapse" aria-labelledby="heading4"
							data-bs-parent="#accordionFooter">
							<div class="accordion-body">
								<ul class="p-0 m-0 namad">
									<li class="">
										<!--<a href="">-->
										<!--	<img src="{{asset('assets/site/images/kaj/psr.jpg')}}" alt="">-->
										<!--</a>-->
										<!--<a referrerpolicy="origin" target="_blank" href="https://trustseal.enamad.ir/?id=237212&amp;Code=eF5odJmk8n3NmaA3r0et">-->
        		<!--						    <img referrerpolicy="origin" src="{{asset('assets/site/images/kaj/nmd.jpg')}}" alt="" style="cursor:pointer" id="eF5odJmk8n3NmaA3r0et">-->
        		<!--					    </a>-->
        		<!--<a referrerpolicy="origin" target="_blank" href="https://trustseal.enamad.ir/?id=237212&amp;Code=eF5odJmk8n3NmaA3r0et"><img referrerpolicy="origin" src="https://Trustseal.eNamad.ir/logo.aspx?id=237212&amp;Code=eF5odJmk8n3NmaA3r0et" alt="" style="cursor:pointer" id="eF5odJmk8n3NmaA3r0et"></a>-->
								
									</li>
									<!--<li class="">-->
									<!--	<a href="">-->
									<!--		<img src="{{asset('assets/site/images/kaj/nmd.jpg')}}" alt="">-->
									<!--	</a>-->
									<!--</li>-->
									<!--<li class="">-->
									<!--	<a href="">-->
									<!--		<img src="{{asset('assets/site/images/kaj/ath.jpg')}}" alt="">-->
									<!--	</a>-->
									<!--</li>-->
								</ul>
							</div>
						</div>
					</div>
					<div class="accordion-item">
						<p class="accordion-header h2" id="heading5">
							<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
								data-bs-target="#collapsefooter6" aria-expanded="false"
								aria-controls="collapsefooter6">
								صفحات اجتماعی
							</button>
						</p>
						<div id="collapsefooter6" class="accordion-collapse collapse" aria-labelledby="heading5"
							data-bs-parent="#accordionFooter">
							<div class="accordion-body">
								<ul class="p-0 mx-auto my-4 d-flex max-content">
									<li class="list-unstyled">
								<a href="https://www.instagram.com/tanoor/" class="bg-white d-flex mx-1 p-2 rounded-circle"
									rel="noopener noreferrer nofollow">
									<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-instagram text-one" viewBox="0 0 16 16">
  <path d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z"/>
</svg>
								</a>
							</li>
					        <li class="list-unstyled">
								<a href="https://t.me/tanoor" class="bg-white d-flex mx-1 p-2 rounded-circle"
									rel="noopener noreferrer nofollow">
									<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-telegram" viewBox="0 0 16 16">
  <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.287 5.906c-.778.324-2.334.994-4.666 2.01-.378.15-.577.298-.595.442-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294.26.006.549-.1.868-.32 2.179-1.471 3.304-2.214 3.374-2.23.05-.012.12-.026.166.016.047.041.042.12.037.141-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8.154 8.154 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629.093.06.183.125.27.187.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.426 1.426 0 0 0-.013-.315.337.337 0 0 0-.114-.217.526.526 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09z"/>
</svg>
</svg>
								</a>
							</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 text-center px-2 py-3">
				
					<p class="text-white">
						کلیه حقوق مادی و معنوی این سایت برای شرکت کابین مبتکر محفوظ می باشد و هرگونه کپی برداری
						پیگرد قانونی دارد
					</p>

				</div>
			</div>
		</div>
	</div>
</footer>
